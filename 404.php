<?php get_header(); ?>

	<div class="error-page">

		<h1><?php the_field('error_page_title','option'); ?></h1>

    <?php the_field('error_page_text','option'); ?>

	</div>
	
<?php get_footer(); ?>