    </main>
    <!--</END MAIN>-->
    <footer class="footer">
      
      <div class="footer__top">
        
        <div class="footer__nav">

          <ul class="footer__social">
            <?php if(get_field('facebook_link','option')) { ?>
            <li><a href="<?php the_field('facebook_link','option'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <?php } ?>
            <?php if(get_field('twitter_link','option')) { ?>
            <li><a href="<?php the_field('twitter_link','option'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <?php } ?>
            <?php if(get_field('instagram_link','option')) { ?>
            <li><a href="<?php the_field('instagram_link','option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <?php } ?>
          </ul>

          <?php wp_nav_menu( array('theme_location' => 'footer_left_menu', 'container' => '', 'menu_class' => 'footer__supp')); ?>
        
        </div>

        <?php wp_nav_menu( array('theme_location' => 'footer_right_menu', 'container_class' => 'footer__terms')); ?>

      </div>

      <div class="footer__bottom">

        <div class="footer__bottom-inner">
          <?php the_field('copyright_line','option'); ?>
        </div>
        
      </div>

    </footer>
  
    <?php wp_footer(); ?>
    
  </body>

</html>