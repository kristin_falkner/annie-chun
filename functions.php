<?php

// Theme Support
require_once(get_template_directory().'/functions/theme-support.php'); 

// WordPress head cleanup
require_once(get_template_directory().'/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/functions/enqueue-scripts.php'); 

// Admin modifications (custom login, editor styles, footer text)
require_once(get_template_directory().'/functions/admin.php'); 

// Disable emojis
require_once(get_template_directory().'/functions/disable-emojis.php'); 

// Comments
require_once(get_template_directory().'/functions/comments.php');

// Register nav menu(s)
require_once(get_template_directory().'/functions/nav-menus.php'); 

// Register custom post type(s)
require_once(get_template_directory().'/functions/custom-post-types.php');

// Register custom image size(s)
require_once(get_template_directory().'/functions/custom-image-sizes.php');

// ACF Options
require_once(get_template_directory().'/functions/acf-options.php'); 

// FontAwesome
require_once(get_template_directory().'/functions/font-awesome.php');

// Gravity Forms
require_once(get_template_directory().'/functions/gravity-forms.php'); 

?>