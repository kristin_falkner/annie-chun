<?php
// ACF Options
if(function_exists('acf_add_options_page')) { 
  acf_add_options_page();
}

// limit ACF field
add_action('admin_print_scripts','anniechuns_inline_scripts', 20, 2);
function anniechuns_inline_scripts() { ?>
<script>
jQuery(document).ready(function(){
  var bol = jQuery('.acf-field-589b62abcd50a input[type="checkbox"]:checked').length >= 4;     
  jQuery('.acf-field-589b62abcd50a input[type="checkbox"]').not(':checked').attr('disabled',bol);

  var bolalt = jQuery('.acf-field-58a1fd4e74ec9 input[type="checkbox"]:checked').length >= 4;     
  jQuery('.acf-field-58a1fd4e74ec9 input[type="checkbox"]').not(':checked').attr('disabled',bolalt);

  jQuery('.acf-field-589b62abcd50a input[type="checkbox"]').on('click', function() {
     var bol = jQuery('.acf-field-589b62abcd50a input[type="checkbox"]:checked').length >= 4;     
     jQuery('.acf-field-589b62abcd50a input[type="checkbox"]').not(':checked').attr('disabled',bol);
  });
  jQuery('.acf-field-58a1fd4e74ec9 input[type="checkbox"]').on('click', function() {
     var bolalt = jQuery('.acf-field-58a1fd4e74ec9 input[type="checkbox"]:checked').length >= 4;     
     jQuery('.acf-field-58a1fd4e74ec9 input[type="checkbox"]').not(':checked').attr('disabled',bolalt);
  });
});
</script>
<?php }