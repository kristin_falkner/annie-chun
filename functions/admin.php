<?php 
// Footer text modification
function remove_footer_admin () {
		echo 'Theme developed by <a href="http://freshmadebrands.com/" target="_blank">freshmade</a> and powered by <a href="http://wordpress.org" target="_blank">WordPress</a>.';
}
add_filter('admin_footer_text', 'remove_footer_admin');

// Custom login styling
function theme_specific_login_style() {
    wp_enqueue_style( 'theme-specific-login', get_template_directory_uri() . '/css/login.css' ); 
}
add_action( 'login_enqueue_scripts', 'theme_specific_login_style' );

// Editor styling
function theme_specific_add_editor_styles() {
    add_editor_style( 'css/admin.css' );
}
add_action( 'admin_init', 'theme_specific_add_editor_styles' );
