<?php
// add custom image sizes
add_image_size( 'background-max', 2880, 1360, true );
add_image_size( 'background-mobile', 1000, 470, true );
add_image_size( 'press-thumb', 560, 280, true );
add_image_size( 'product-thumb', 360, 360, true );
add_image_size( 'content-wide', 1000, 500, true );