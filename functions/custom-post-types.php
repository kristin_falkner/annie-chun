<?php
// let's create the function for the custom type
function recipes_cpt() { 
	// creating (registering) the custom type 
	register_post_type( 'recipes', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Recipes', 'annie-chun'), /* This is the Title of the Group */
			'singular_name' => __('Recipe', 'annie-chun'), /* This is the individual type */
			'all_items' => __('All Recipes', 'annie-chun'), /* the all items menu item */
			'add_new' => __('Add New', 'annie-chun'), /* The add new menu item */
			'add_new_item' => __('Add New Recipe', 'annie-chun'), /* Add New Display Title */
			'edit' => __( 'Edit', 'annie-chun' ), /* Edit Dialog */
			'edit_item' => __('Edit Recipes', 'annie-chun'), /* Edit Display Title */
			'new_item' => __('New Recipe', 'annie-chun'), /* New Display Title */
			'view_item' => __('View Recipe', 'annie-chun'), /* View Display Title */
			'search_items' => __('Search Recipes', 'annie-chun'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'annie-chun'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'annie-chun'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'These are recipes', 'annie-chun' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-carrot', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'recipes', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail')
		) /* end of options */
	); /* end of register post type */
	
}

function products_cpt() { 
	// creating (registering) the custom type 
	register_post_type( 'product', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Products', 'annie-chun'), /* This is the Title of the Group */
			'singular_name' => __('Product', 'annie-chun'), /* This is the individual type */
			'all_items' => __('All Products', 'annie-chun'), /* the all items menu item */
			'add_new' => __('Add New', 'annie-chun'), /* The add new menu item */
			'add_new_item' => __('Add New Product', 'annie-chun'), /* Add New Display Title */
			'edit' => __( 'Edit', 'annie-chun' ), /* Edit Dialog */
			'edit_item' => __('Edit Product', 'annie-chun'), /* Edit Display Title */
			'new_item' => __('New Product', 'annie-chun'), /* New Display Title */
			'view_item' => __('View Product', 'annie-chun'), /* View Display Title */
			'search_items' => __('Search Product', 'annie-chun'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'annie-chun'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'annie-chun'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'These are products', 'annie-chun' ), /* Custom Type Description */
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-cart', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'product', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail' )
		) /* end of options */
	); /* end of register post type */
	
}

function press_cpt() { 
	// creating (registering) the custom type 
	register_post_type( 'press', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Press', 'annie-chun'), /* This is the Title of the Group */
			'singular_name' => __('Press Item', 'annie-chun'), /* This is the individual type */
			'all_items' => __('All Press', 'annie-chun'), /* the all items menu item */
			'add_new' => __('Add New', 'annie-chun'), /* The add new menu item */
			'add_new_item' => __('Add New Press Item', 'annie-chun'), /* Add New Display Title */
			'edit' => __( 'Edit', 'annie-chun' ), /* Edit Dialog */
			'edit_item' => __('Edit Press', 'annie-chun'), /* Edit Display Title */
			'new_item' => __('New Press', 'annie-chun'), /* New Display Title */
			'view_item' => __('View Press', 'annie-chun'), /* View Display Title */
			'search_items' => __('Search Press', 'annie-chun'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'annie-chun'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'annie-chun'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'These are press items', 'annie-chun' ), /* Custom Type Description */
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 9, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-megaphone', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'press', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor' )
		) /* end of options */
	); /* end of register post type */
	
}

// adding the function to the Wordpress init
add_action( 'init', 'recipes_cpt');
add_action( 'init', 'products_cpt');
add_action( 'init', 'press_cpt');

// register taxonomies for recipes + products
register_taxonomy( 'products', 
	array('product'), 
	array(
		'hierarchical' => true,     /* if this is true, it acts like categories */             
		'labels' => array(
			'name' => __( 'Product Groups', 'annie-chun' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Product Group', 'annie-chun' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Product Groups', 'annie-chun' ), /* search title for taxomony */
			'all_items' => __( 'All Product Groups', 'annie-chun' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Product Group', 'annie-chun' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Product Group:', 'annie-chun' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Product Group', 'annie-chun' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Product Group', 'annie-chun' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Product Group', 'annie-chun' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Product Group', 'annie-chun' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'products' ),
	)
);

register_taxonomy( 'recipe-type', 
	array('recipes'), 
	array(
		'hierarchical' => true,     /* if this is true, it acts like categories */             
		'labels' => array(
			'name' => __( 'Recipe Types', 'annie-chun' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Recipe Type', 'annie-chun' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Recipe Types', 'annie-chun' ), /* search title for taxomony */
			'all_items' => __( 'All Recipe Types', 'annie-chun' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Recipe Type', 'annie-chun' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Recipe Type:', 'annie-chun' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Recipe Type', 'annie-chun' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Recipe Type', 'annie-chun' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Recipe Type', 'annie-chun' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Recipe Type', 'annie-chun' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'recipe-type' ),
	)
);