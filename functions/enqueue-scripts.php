<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  
    //pull jQuery from Google CDN
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery',   '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', array(), '1.12.4', false );
    
    // Adding modernizr
    wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.6.2.min.js', '2.6.2', false);
    
    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/js/theme.min.js', array( 'jquery' ), filemtime(get_stylesheet_directory() . '/js/theme.min.js'), true );

    // Google Fonts
    wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Cabin:400,400i,600,700|Fjalla+One');
    wp_enqueue_style( 'googleFonts');

    // Add product locator
    if(is_page_template('pages/where-to-buy.php')) {
      wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAbAuPQsYSV8XSfq6tjI0Ndmm-I6sRUp-Y', array(), '', false );
      wp_enqueue_script( 'product-locator', get_template_directory_uri() . '/js/vendor/pl_widget.js', array( 'jquery' ), '1.0', false );
    }
   
    // Register and add minified stylesheet
    wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/style.min.css', array(), filemtime(get_stylesheet_directory() . '/style.min.css')); 

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);