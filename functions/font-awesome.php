<?php 
//Load in FontAwesome
function custom_enqueue_awesome() {
  wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/fa464c9cf8.js', array(), '4.7.0' );
}
add_action( 'wp_enqueue_scripts', 'custom_enqueue_awesome' );