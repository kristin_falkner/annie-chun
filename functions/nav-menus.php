<?php 
// add specific WP menus
register_nav_menus( array(
  'main_menu' => 'Main Menu',
  'footer_left_menu' => 'Footer Left Menu',
  'footer_right_menu' => 'Footer Right Menu',
  'mobile_menu' => 'Mobile Menu'
) );