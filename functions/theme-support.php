<?php
	
// Adding WP Functions & Theme Support
function starter_theme_support() {

	// Add title tag
	add_theme_support( 'title-tag' );

	// Add WP Thumbnail Support
	add_theme_support( 'post-thumbnails' );

	// Add RSS Support
	add_theme_support( 'automatic-feed-links' );
	
	// Add HTML5 Support
	add_theme_support( 'html5', 
					 array( 
						'comment-list', 
						'comment-form', 
						'search-form', 
					 ) 
	);

	// Set the maximum allowed width for any content in the theme, like oEmbeds and images added to posts.
	$GLOBALS['content_width'] = apply_filters( 'starter_theme_support', 1200 );	
	
} /* end theme support */

add_action( 'after_setup_theme', 'starter_theme_support' );

// add svg support
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function bodhi_svgs_disable_real_mime_check( $data, $file, $filename, $mimes ) {
		$wp_filetype = wp_check_filetype( $filename, $mimes );

		$ext = $wp_filetype['ext'];
		$type = $wp_filetype['type'];
		$proper_filename = $data['proper_filename'];

		return compact( 'ext', 'type', 'proper_filename' );
}
add_filter( 'wp_check_filetype_and_ext', 'bodhi_svgs_disable_real_mime_check', 10, 4 );