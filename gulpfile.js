// Grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    pixrem = require('gulp-pixrem'),
    imageresize = require('gulp-image-resize'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create();

// Compile Sass, Autoprefix and minify
gulp.task('styles', function() {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(plumber(function(error) {
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
        }))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
            cascade: true
        }))
        .pipe(pixrem())
        .pipe(gulp.dest('./'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssnano({ zindex: false }))
        .pipe(gulp.dest('./'))
});
    
// JSHint, concat, and minify JavaScript
gulp.task('site-js', function() {
  return gulp.src([ 
      
           // Grab your custom scripts
          './src/scripts/*.js'
          
  ])
    .pipe(plumber())
    .pipe(concat('theme.js'))
    .pipe(gulp.dest('./js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./js'))
}); 

// Minify images
gulp.task('imgminify', function() {
    return gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./images'));
});

// Browser-Sync watch files and inject changes
gulp.task('browsersync', function() {
    // Watch files
    var files = [
        './*.css', 
        './js/*.js',
        '**/*.php',
        'images/**/*.{png,jpg,gif,svg,webp}',
    ];

    browserSync.init(files, {
        // Replace with URL of your local site
        proxy: "http://localhost/",
    });
    
    gulp.watch('./src/sass/**/*.scss', ['styles']);
    gulp.watch('./src/images/**/*.{png,jpg,gif,svg,webp}', ['imgminify']);
    gulp.watch('./src/scripts/*.js', ['site-js']).on('change', browserSync.reload);

});

// Watch files for changes (without Browser-Sync)
gulp.task('watch', function() {

  // Watch .scss files
  gulp.watch('./src/sass/**/*.scss', ['styles']);

  // Watch site-js files
  gulp.watch('./src/scripts/*.js', ['site-js']);

  // Watch image files
  gulp.watch('./src/images/**/*.{png,jpg,gif,svg,webp}', ['imgminify']);

}); 

// Run styles, site-js
gulp.task('default', function() {
  gulp.start('styles', 'site-js', 'imgminify');
});
