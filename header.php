<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="utf-8">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<?php
		if ( ! function_exists( '_wp_render_title_tag' ) ) {
			function theme_slug_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
			}
			add_action( 'wp_head', 'theme_slug_render_title' );
		}
		?>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/images/apple-icon-touch.png" rel="apple-touch-icon" />
			<!--[if IE]>
				<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
			<![endif]-->
			<meta name="msapplication-TileColor" content="#ffffff">
			<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png">
			<meta name="theme-color" content="#424242">
		<?php } ?>
	
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<?php wp_head(); ?>
		
		<?php if(is_front_page()) { ?>
		<style>
			@media (min-width: 768px) {
				<?php $i = 1; if( get_field('slides') ): while( has_sub_field('slides') ): 
				$slide_image = get_sub_field('slide_image');
				$mobile_slide_image = get_sub_field('slide_image_mobile'); ?>
				.homepage-slide-image-<?php echo $i; ?> {
					background-image: url(<?php echo $slide_image['sizes']['background-max']; ?>);
				}
				<?php $i++; endwhile; endif; ?>
			}

			@media (max-width: 767px) {
				<?php $i = 1; if( get_field('slides') ): while( has_sub_field('slides') ): 
				$slide_image = get_sub_field('slide_image');
				$mobile_slide_image = get_sub_field('slide_image_mobile'); ?>
				.homepage-slide-image-<?php echo $i; ?> {
					background-image: url(<?php echo $mobile_slide_image['sizes']['background-mobile']; ?>);
				}
				<?php $i++; endwhile; endif; ?>
			}
		</style>
		<?php } elseif(is_singular('recipes')) { ?>
		<style>
			
			.recipe__hero-box h1,
			.recipe__content h2 {
				color: <?php the_field('recipe_color'); ?>;
			}

			.recipe__bar {
				background: <?php the_field('recipe_color'); ?>;
			}

			@media (min-width: 768px) {
				.hero {
					background-image: url(<?php the_post_thumbnail_url('background-max'); ?>);
				}
			}

			@media (max-width: 767px) {
				.hero {
					background-image: url(<?php the_post_thumbnail_url('background-mobile'); ?>);
				}
			}

		</style>
		<?php } elseif(is_tax('products')) {
			// vars
			$queried_object = get_queried_object(); 
			$taxonomy = $queried_object->taxonomy;
			$term_id = $queried_object->term_id;
			$hero_image = get_field('hero_image', $taxonomy . '_' . $term_id);  ?>
		<style>
			
			@media (min-width: 768px) {
				.hero {
					background-image: url(<?php echo $hero_image['sizes']['background-max']; ?>);
				}
			}

			@media (max-width: 767px) {
				.hero {
					background-image: url(<?php echo $hero_image['sizes']['background-mobile']; ?>);
				}
			}

		</style>
		<?php } elseif(is_page_template('pages/story.php')) {
		$herobg = get_field('mobile_hero_image');
		$lowerbg = get_field('lower_background_image');
		$separator = get_field('hero_body_separator_image');
		$section1img = get_field('section_1_image');
		$section2img = get_field('section_2_image');
		$section3img = get_field('section_3_image'); ?>
		<style>

			@media (min-width: 640px) {
				.hero {
					background-image: url(<?php the_post_thumbnail_url('background-max'); ?>);
				}

				.story__endcap {
					background-image: url(<?php echo $lowerbg['sizes']['background-max']; ?>);
				}

				.story__separator {
					background-image: url(<?php echo $separator['sizes']['background-max']; ?>);
				}

				.story__section-image-1 {
					background-image: url(<?php echo $section1img['url']; ?>);
				}

				.story__section-image-2 {
					background-image: url(<?php echo $section2img['url']; ?>);
				}

				.story__section-image-3 {
					background-image: url(<?php echo $section3img['url']; ?>);
				}
			}

			@media (max-width: 639px) {

				.hero {
					background-image: url(<?php echo $herobg['sizes']['background-max']; ?>);
				}
				
				.story__endcap {
					background-image: url(<?php echo $lowerbg['sizes']['background-mobile']; ?>);
				}

				.story__separator {
					background-image: url(<?php echo $separator['sizes']['large']; ?>);
				}

				.story__section-image-1 {
					background-image: url(<?php echo $section1img['sizes']['large']; ?>);
				}

				.story__section-image-2 {
					background-image: url(<?php echo $section2img['sizes']['large']; ?>);
				}
			}

		</style>
		<?php } elseif(is_page_template('pages/where-to-buy.php')) { ?>
		<script>  
		var pl_params = {
		container: 'TOOL',
		client_id: 286,
		brand_id: 'ANCH',
		default_product : '6566710090',
		product_tabs:[ 
		{ 'name':'SOUP AND NOODLE BOWLS',    'groups':['A1'] } ,
		{ 'name':'SNACKS',    'groups':['A2'] } ,
		{ 'name':'RICE/NOODLE',    'groups':['A3'] } ,
		{ 'name':'DUMPLINGS',    'groups':['A4'] },
		{ 'name':'SAUCES',    'groups':['A5'] } ], 
		product_images:[

									 { 'product_id':'A1', 'image':'http://anniechun.com/wp-content/uploads/2016/10/AC_WebsiteImages_100H_SBNB.png',     'position':'top', 'text':'hide', 'class':'pl_image_style1'}
								 ,
									 { 'product_id':'A2', 'image':'http://anniechun.com/wp-content/uploads/2016/10/AC_WebsiteImages_100H_OrgSSSC.png',     'position':'top', 'text':'hide', 'class':'pl_image_style2'}
								 ,
									 { 'product_id':'A3', 'image':'http://anniechun.com/wp-content/uploads/2016/11/AC_WebsiteImages_100H_RXOrgUdon1.png',     'position':'top', 'text':'hide', 'class':'pl_image_style3'}
								 ,
									 { 'product_id':'A4', 'image':'http://anniechun.com/wp-content/uploads/2016/10/AC_WebsiteImages_100H_MWOrgPS.png',     'position':'top', 'text':'hide', 'class':'pl_image_style4'}
								 ,
									 { 'product_id':'A5', 'image':'http://anniechun.com/wp-content/uploads/2016/10/AC_WebsiteImages_100H_Sauces.png',     'position':'top', 'text':'hide', 'class':'pl_image_style5'}
		],       
	};
		</script>
		<?php } elseif(is_page() && !is_home()) { ?>
		<style>

			@media (min-width: 768px) {
				.hero {
					background-image: url(<?php the_post_thumbnail_url('background-max'); ?>);
				}
			}

			@media (max-width: 767px) {
				.hero {
					background-image: url(<?php the_post_thumbnail_url('background-mobile'); ?>);
				}
			}

		</style>
		<?php } ?>
		
		<!--Google Analytics-->
		
		<!--End Google Analytics-->

	</head>

	<body <?php body_class(); ?>>

		<a class="screen-reader-text" href="#content">Skip to main content</a>
	
		<header class="header">

			<div class="header__inner">
				
				<a class="header__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo bloginfo('name'); ?></a>

				<ul class="header__social">
					<?php if(get_field('facebook_link','option')) { ?>
					 <li><a href="<?php the_field('facebook_link','option'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
					 <?php } ?>
					 <?php if(get_field('twitter_link','option')) { ?>
					 <li><a href="<?php the_field('twitter_link','option'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
					 <?php } ?>
					 <?php if(get_field('instagram_link','option')) { ?>
					 <li><a href="<?php the_field('instagram_link','option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
					 <?php } ?>
				</ul>

				<?php wp_nav_menu( array('theme_location' => 'main_menu','container_class' => 'header__nav')); ?>

				<a class="header__mobile">
					<div class="header__icon">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</a>

			</div>
		
		</header>

		<div class="mobile-menu">
			
			<?php wp_nav_menu( array('theme_location' => 'mobile_menu', 'container_class' => 'mobile-menu__nav')); ?>

			<?php wp_nav_menu( array('theme_location' => 'footer_right_menu', 'container_class' => 'mobile-menu__supp')); ?>

			<ul class="mobile-menu__social">
				<?php if(get_field('facebook_link','option')) { ?>
					<li><a href="<?php the_field('facebook_link','option'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
				<?php } ?>
				<?php if(get_field('twitter_link','option')) { ?>
					<li><a href="<?php the_field('twitter_link','option'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<?php } ?>
				<?php if(get_field('instagram_link','option')) { ?>
					<li><a href="<?php the_field('instagram_link','option'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
				<?php } ?>
			</ul>

		</div>
		
		<main id="content">