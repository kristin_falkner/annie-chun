<?php get_header(); ?>

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); 
		
			get_template_part('parts/loop','archive');	
		
		endwhile; 
			
			get_template_part('parts','page-navigation');
		
		else : 
		
			get_template_part('parts/loop','missing');
			
	endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>