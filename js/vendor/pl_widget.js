/*
  IRI Product Locator
  Copyright 2015 Information Resources Inc. All rights reserved.

  To initialize the Product Locator Tab Search Tool:

  var pl_params = {
    container 'divID'
  , client_id: 103
  , brand_id: "DEMO"
  , product_tabs:[ { "id":"G1", "name":"Group 1"  }, { "id":"G2", "name":"Group 2"  }]
  , groups: [ tab1 : { "G1", "G2" } ]
  , group_logos: { "G1":"logo.jpg", "G2":"g2logo.jpg" }
  };

*/
(function () {
    var pl_base_url = '//productlocator.iriworldwide.com/productlocator/';

    function loadScript(url, callback) {
        var script = document.createElement("script")
        script.type = "text/javascript";
        if (script.readyState) { //IE
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else { //Others
            script.onload = function () {
                callback();
            };
        }
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    }

    function loadFile( tag, options ) {
      var element = document.createElement(tag);
      var firstScript = document.getElementsByTagName('script')[0];
      for(var attr in options) element[attr] = options[attr];
      firstScript.parentNode.insertBefore(element,firstScript);
    }

    loadFile('link',{'href':pl_base_url+'/css/IRIProductLocator.css','rel':'stylesheet'});
    //loadFile('link',{'href':pl_base_url+'/css/bootstrap-theme.min.css','rel':'stylesheet'});
    //loadFile('link',{'href':pl_base_url+'/css/bootstrap.min.css','rel':'stylesheet'});

    loadScript(pl_base_url + 'js/jquery.js', function () {
         //jQuery loaded
         //console.log('jquery.js loaded');
         loadScript(pl_base_url + 'js/bootstrap.min.js', function () {
           //console.log('bootstrap.min.js loaded');
           loadScript(pl_base_url + 'js/bootstrap-tabcollapse.js', function () {
             //console.log('bootstrap-tabcollapse.js loaded');
             var pl = new pl_widget( pl_params );
           });
         });

    });

    function findObjectByID(objArray, id) {
      if ( objArray == null || id == null ) { return null; }
        for (var i = 0, len = objArray.length; i < len; i++) {
          if (objArray[i].product_id === id)
            return objArray[i];
      }
      return null;
    }

    function debounce(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    };

function pl_widget(params) {
  //console.log('method -- constructor');
  this.params = params;
  this.container = $('#' + this.params.container) || $('<div>', { 'id' : 'pl_widget'}).appendTo( $('body') );
  this.parentContainer = this.container.parent();
  this.baseURL = pl_base_url;
  this.step = 'search';
  this.search_type = 'zip';
  this.default_product = params.default_product || 'all';
  if (params.client_id != null) {
    this.client_id = params.client_id;
  } else this.criticalError('client_id not provided.');
  if (params.brand_id != null) {
    this.brand_id = params.brand_id;
  } else this.criticalError('brand_id not provided.');

  //Fix for mobile map layout
  var resizeFtn = debounce(function() {
    var mapDiv = $('#pl_map_content');
    if ( $( window ).width() >= 768 ) {
      mapDiv.addClass('pl_regular_view');
      mapDiv.removeClass('pl_16x9_view');
    } else {
      mapDiv.addClass('pl_16x9_view');
      mapDiv.removeClass('pl_regular_view');
    }
  },100);
  $( window ).resize(resizeFtn);

  this.setSession();
  this.render();
}

pl_widget.prototype = {
      constructor: pl_widget
//--------------------------------------------------------------------------------------------------------------------------
    , render: function () {
      //console.log('method -- render');
      var callback = this;

      this.bscontainer = $('<div>', {'id': 'bscontainer', 'class':'container-fluid'}).appendTo(this.container);
      if ($('meta[name=viewport]').length == 0) {
        $('<meta>', {'name': 'viewport', 'content':'width=device-width, initial-scale=1'}).appendTo( $('head') );
      }

      //Location
      row = $('<div>', {'class':'row'}).appendTo(this.bscontainer);      //REMOVE , 'class':'visible-sm visible-md visible-lg'
      reg_location = $('<div>', {'id': 'pl_location'}).appendTo(row); //REMOVE .css({'text-align':'center','vertical-align':'middle'});
      this.setLocation(reg_location);
      this.getLocation();

      //Build Search Page
      main = $('<div>', {'class':'row'}).appendTo(this.bscontainer);
      this.searchPage = $('<div>', {'id': 'pl_searchPage','class':'col-xs-12'}).appendTo(main);

      var tabs = $('<div>',{'id':'pl_tabs','class':'container'}).appendTo(this.searchPage);  //REMOVE .css({'margin-top':'10px','display':'none'});
      var tab_list = $('<ul>',{'id':'pl_tab_list','class':'nav nav-tabs nav-justified','role':'tablist'}).appendTo(tabs);
      var tabs_content = $('<div>',{'id':'pl_tabs_content','class':'tab-content'}).appendTo(tabs);

      if ( this.params.hasOwnProperty('product_tabs') ) {
        $.each(this.params.product_tabs, function(idx,item) {
          var tab = $('<li>',{'id':'tabbtn'+idx.toString(),'role':'presentation'}).appendTo(tab_list);
          $('<a>',{'href':'#tab'+idx.toString(),'aria-controls':'tab'+idx.toString(),'role':'tab','data-toggle':'tab','class':'text-nowrap'}).appendTo(tab).html('<p>'+item.name+'</p>');
          $('<div>',{'id':'tab'+idx.toString(),'role':'tabpanel','class':'tab-pane pl_product_content'}).appendTo(tabs_content); //REMOVE .css({'text-align':'center','padding':'0'});
        });
      }

      //Build Results Page
      this.resultsPage = $('<div>', {'id': 'pl_resultsPage','class':'col-xs-12 pl_nopadding'}).appendTo(main);  //REMOVE .css('display','none');
      this.resultsToolBar = $('<div>', {'id':'pl_resultsToolBar'}).appendTo(this.resultsPage); //REMOVE .css({'width':'100%','background-color':'lightgrey'});
      backbtn = $('<button>',{'type':'button','class':'btn btn-default btn-xs'}).html('<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>New Search').appendTo(this.resultsToolBar);
      backbtn.click(function(){
        delete callback.radius;
        callback.changeProducts();
      });

      var map_class = 'pl_16x9_view';
      if ( $( window ).width() > 768 ) { map_class = 'pl_regular_view'; }
      this.mapContent = $('<div>', {'id':'pl_map_content','class':'col-sm-8 col-xs-12 col-sm-push-4 col-xs-height '+map_class}).appendTo(this.resultsPage); //REMOVE .html('map-content'); //REMOVE .css({'border':'2px solid', 'border-color':'green','height':'500px'});
      this.storeList = $('<div>', {'id':'pl_store_list','class':'col-sm-4 col-xs-12 col-sm-pull-8 pl_nopadding'}).appendTo(this.resultsPage); //REMOVE .css({'border':'2px solid', 'border-color':'red','height':'500px'});
      //REMOVE this.storeList.css('overflow-y','scroll');
      $('<br>').appendTo(this.bscontainer);

      //Footer
      row = $('<div>', {'class':'row'}).appendTo(this.bscontainer);
      this.setFooter($('<div>', {'id': 'footer-lg', 'class':'visible-lg'}).appendTo(row));

      this.renderStoreDetails(main);
      this.fetchProducts();
    }
//--------------------------------------------------------------------------------------------------------------------------
    , setHeader: function (div) {
        //console.log('method -- setHeader');
        div.html('Where to Buy');
        $('<hr>').appendTo(div);
    }
//--------------------------------------------------------------------------------------------------------------------------
    , setLocation: function (div) {
        //console.log('method -- setLocation');
        var callback = this;

        $('<p>',{'id':'pl_where_to_buy_title','class':'pl_inline'}).appendTo(div).html("Where To Buy Near"); //REMOVE.css({'margin-top':'20px','font-weight':'bold','font-size':'20pt'});

        grp = $('<div>',{'id':'pl_zipcontrol','class':'form-group pl_inline'}).appendTo(div);
        ipt = $('<input>',{'id':'pl_zip','type':'text','class':'form-control','placeholder':'zip code','maxlength':'5','title':'Please provide a 5-digit zip code','data-placement':'top'}).appendTo(grp); //REMOVE .css({'margin-right':'10px','margin-left':'10px','width':'80px','text-align':'center'});

        btn = $('<button>',{'id':'pl_find_btn','type':'button','class':'btn btn-default pl_inline pl_find_button'}).html('<span class="glyphicon glyphicon-search" aria-hidden="true"></span> Find').appendTo(div);
        btn.click(function(){
          callback.findLocations();
        });

        $('<hr>',{'id':'header_bottom_border'}).appendTo(div); //REMOVE .css({'margin-top':'0px','margin-bottom':'5px'});
    }
//--------------------------------------------------------------------------------------------------------------------------
    , setFooter: function (div) {
        //console.log('method -- setFooter');
        $('<hr>').appendTo(div);
        $('<img>',{ 'id': 'pl_iri_logo', 'src': './images/iri_logo.png', 'class':'img-responsive'}).appendTo(div);
    }
//--------------------------------------------------------------------------------------------------------------------------
    , changeProducts: function (div) {
      if (this.step == 'results') {
        this.resultsPage.fadeOut('slow');
        this.searchPage.fadeIn('slow');
        this.step = 'search';
      }
    }
//--------------------------------------------------------------------------------------------------------------------------
    , findLocations: function () {
      //console.log('method -- switchPage ' + this.step);

      //detect zip vs gps
      if (this.search_type == 'zip') {
        var zip = $('#pl_zip').val();
        var zipRegex = /^\d{5}$/;
        if (!zipRegex.test(zip)) {
          $('#pl_zipcontrol').toggleClass("has-error");
          $('#pl_zip').popover('show');
          return;
        } else {
          $('#pl_zipcontrol').removeClass('has-error');
          this.fetchZip(zip);
          this.zip = zip;
        }
      } else {
        callback.createMap();
      }

      if (this.step == 'search') {
        this.searchPage.fadeOut('slow');
        this.resultsPage.fadeIn('slow');
        this.step = 'results';
      }
     
      //fix for research grey map
      if(this.map != undefined){
        this.map.setZoom(12);
          google.maps.event.trigger(this.map, "resize");
          this.map.panTo(this.centerPnt);
      }
      
      this.storeList.empty();
      this.fetchStores();
    }
//--------------------------------------------------------------------------------------------------------------------------
    , getLocation: function() {
      callback = this;
      var onSuccess = function(position) {
        callback.latitude  = position.coords.latitude;
        callback.longitude = position.coords.longitude;
        //change zip input to GPS icon

        div = $('<div>',{'id':'pl_gps_label'}).html('GPS&nbsp;'); //REMOVE .css({'padding-left':'20px','padding-right':'20px','font-size':'16pt'});
        $('<span>',{'id':'gps','class':'glyphicon glyphicon-screenshot','aria-hidden':'true'}).appendTo(div);

        $('#pl_zip').replaceWith( div );
        callback.search_type = 'gps';
      };

      function onError(error) {
        console.log('geolocation - unable to get location: ' + error.message );
      }

      navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }
//--------------------------------------------------------------------------------------------------------------------------
    , fetchProducts: function (param) {
        //console.log('method -- fetchProducts ');
        var callback = this;
        var url = this.baseURL + 'products?client_id=' + this.client_id +'&brand_id=' + this.brand_id + '&output=json-group-list';
        var groups = $.unique($.map(this.params.product_tabs, function(item){ return item.groups }));
        callback.group_list = '';
        $.each(groups, function(idx,item){ callback.group_list += '&group_id=' + item; });
        url += callback.group_list;
        $.getJSON( url, function(data){ callback.updateProducts(data)} );
    }
//--------------------------------------------------------------------------------------------------------------------------
    , fetchStores: function () {
        //console.log('method -- fetchStores');
        this.loadingImage($('#pl_store_list'));
        var callback = this;
        var url = this.baseURL + 'servlet/ProductLocatorEngine?clientid=' + this.client_id +'&productfamilyid=' + this.brand_id + '&producttype=upc&outputtype=json';
        if (typeof this.radius != "undefined") { url += '&searchradius=' + this.radius }
        if (this.search_type == 'zip') { url += '&zip=' + this.zip }
        if (this.search_type == 'gps') { url += '&searchtype=point&latitude=' + this.latitude + '&longitude=' + this.longitude }
        url += '&sid=' + this.session_id;
       
        this.prod_list = $.unique($.map($('input[type=checkbox]:checked'), function(item){return item.id;}));
        //if no product is selected default to 'all'
        if ( this.prod_list.length == 0) {
          url += '&productid=' + this.default_product;
        } else {
          $.each(this.prod_list,function(idx,item){
            url += '&productid=' + item;
          });
          callback.upc_info = new Object();
          $.each( callback.product_info.products.product, function (idx, element) {
            if ( $.inArray(element.upc_code, callback.prod_list) != -1) {
              callback.upc_info[element.upc_code] = element.upc_name;
            }
            if ( $.inArray(element.group_id, callback.prod_list) != -1) {
              callback.upc_info[element.group_id] = element.group_name;
            }
          });
        }

        $.getJSON(
            url
          , function(data){ callback.updateStores(data)
          }
        );
    }
//--------------------------------------------------------------------------------------------------------------------------
    , fetchZip: function (zip) {
        //console.log('method -- fetchZip');
        var callback = this;
        var url = this.baseURL + 'zip?zip=' + zip;
        $.getJSON( url, function(data){
              if( data.error == null) {
                callback.latitude = data.latitude;
                callback.longitude = data.longitude;
                callback.createMap();
              }
          }
        );
    }
//--------------------------------------------------------------------------------------------------------------------------
    , setSession: function () {
        var callback = this;
        var url = this.baseURL + 'uniqueid';
        $.getJSON( url, function(data){
              if( data.error == null) {
                callback.session_id = data.GUID;
              }
          }
        );
    }    
//--------------------------------------------------------------------------------------------------------------------------
    , updateProducts: function (data) {
        //console.log('method -- updateProducts ');
        var callback = this;
        callback.product_info = data;
        if( data.products != null) {

          // Update Tabs
          $.each(this.params.product_tabs, function(tab_idx,tab_item) {
            var tab = $('#tab'+tab_idx.toString());
            var collapse = ( tab_item.hasOwnProperty('collapse') && tab_item.collapse == 'true');

            if (collapse) {
              var dupes = {};
              var products = $.map(data.products.product, function (item) {
                               if (!dupes[item.group_id] && $.inArray(item.group_id, tab_item.groups) != -1 ) {
                                 dupes[item.group_id] = true;
                                 return { 'upc_code':item.group_id, 'upc_name':item.group_name };
                               }
                              });
              if (products.length > 0) { //If groups are found
                var div = $('<span>',{ 'id':'div-' + tab_idx.toString(), 'class':'pl_product_list'}).appendTo(tab);
                $.each(products, function( grp_idx, grp_item) {

                  var group_id   = grp_item.upc_code;
                  var group_name = grp_item.upc_name;

                  var imageObj = findObjectByID(callback.params.product_images,group_id);
                  if( imageObj != null) {

                    if(imageObj.text == 'hide') {
                      //image no text
                      grp_item.upc_name = function (div) {
                        $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(div);
                      }
                    } else {
                      switch( imageObj.position ) {
                      case 'top':
                        //image and text
                        grp_item.upc_name = function (div) {
                          var input = div.children().first();
                          var table = $('<table>').appendTo(div);
                          var row = $('<tr>').appendTo(table);
                          var cell = $('<td>',{'rowspan':'2','class':'pl_prod_cell'}).appendTo(row);
                          $(input).detach().appendTo(cell);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(cell);
                          var row = $('<tr>').appendTo(table);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $('<h3>').appendTo(cell).html(group_name);
                        }
                        break;
                      case 'bottom':
                        //image and text
                        grp_item.upc_name = function (div) {
                          var input = div.children().first();
                          var table = $('<table>').appendTo(div);
                          var row = $('<tr>').appendTo(table);
                          var cell = $('<td>',{'rowspan':'2','class':'pl_prod_cell'}).appendTo(row);
                          $(input).detach().appendTo(cell);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $('<h3>').appendTo(cell).html(group_name);
                          var row = $('<tr>').appendTo(table);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(cell);
                        }
                        break;
                      case 'left':
                        //image and text
                        grp_item.upc_name = function (div) {
                          var input = div.children().first();
                          var table = $('<table>').appendTo(div);
                          var row = $('<tr>').appendTo(table);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $(input).detach().appendTo(cell);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(cell);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $('<h3>').appendTo(cell).html(group_name);
                        }
                        break;
                      case 'right':
                        //image and text
                        grp_item.upc_name = function (div) {
                          var input = div.children().first();
                          var table = $('<table>').appendTo(div);
                          var row = $('<tr>').appendTo(table);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $(input).detach().appendTo(cell);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $('<h3>').appendTo(cell).html(group_name);
                          var cell = $('<td>',{'class':'pl_prod_cell'}).appendTo(row);
                          $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(cell);
                        }
                        break;
                      default:
                        grp_item.upc_name  = function (div) {
                          $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(div);
                          $('<h3>').html(group_name).appendTo(div);
                        }
                      }
                    }
                  }
                });
                callback.renderCheckBox(div, tab_idx.toString(), products);

              }
            } else {
              $.each(tab_item.groups, function(grp_idx,grp_item) {
                var products = $.grep(data.products.product, function(n,i){ return n.group_id === grp_item} );

                if (products.length > 0) { //If group is found with products
                  var div = $('<span>',{ 'id':'div-' + grp_item, 'class':'pl_product_list'}).appendTo(tab); //REMOVE .css({'display':'inline-block','text-align':'left','margin':'10px','vertical-align':'top'});
                  var group_id   = products[0].group_id;
                  var group_name = products[0].group_name;

                  var imageObj = findObjectByID(callback.params.product_images,group_id);
                  if( imageObj != null) {

                    if(imageObj.text == 'hide') {
                      //image no text
                      $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(div);
                    } else {
                      switch( imageObj.position ) {
                      case 'top':
                        //image and text
                        $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(div);
                        $('<h3>').appendTo(div).html(group_name);
                        break;
                      case 'bottom':
                        //image and text
                        $('<h3>').appendTo(div).html(group_name);
                        $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(div);
                        break;
                      case 'left':
                        //image and text
                        $('<img>',{'src':imageObj.image, 'class':imageObj.class+' pl_left'}).appendTo(div);
                        $('<h3>').appendTo(div).html(group_name);
                        break;
                      case 'right':
                        //image and text
                        $('<img>',{'src':imageObj.image, 'class':imageObj.class+' pl_right'}).appendTo(div);
                        $('<h3>').appendTo(div).html(group_name);
                        break;
                      default:
                        $('<img>',{'src':imageObj.image, 'class':imageObj.class}).appendTo(div);
                        $('<h3>').appendTo(div).html(group_name);
                      }
                    }
                  } else {
                    $('<h3>').appendTo(div).html(group_name).css('text-align','left');
                  }

                  callback.renderCheckBox(div, group_id, products);
                }

              });
            } // collapse

          });

        //Rebuild tab collapse stuff
        $('#tabbtn0').addClass('active');
        $('#tab0').addClass('active');
        $('#pl_tab_list').tabCollapse();
        $('#pl_tabs').fadeIn();


        }
    }
//--------------------------------------------------------------------------------------------------------------------------
    , renderCheckBox: function (div, tab_id, products) {
      var list = $('<ul>',{'id':'list-' + tab_id, 'class':'pl_product_checkbox'}).appendTo(div);
      $.each(products, function(prd_idx,prd_item) {
        var tmp = $('<li>').appendTo(list).css({'list-style-type':'none','list-style':'none','padding-left':'0'});
        var label = $('<label>').appendTo(tmp);
        $('<input>',{'type':'checkbox', 'name':prd_item.upc_code, 'id':prd_item.upc_code}).appendTo(label).on('change', function(){
          var id = $(this).attr('id')
          var checked = $(this).is(':checked');
          $( "input[name='" + id + "']" ).attr('checked', checked);

        });
        if (typeof(prd_item.upc_name) == 'string') {
          label.append('&nbsp;&nbsp;' + prd_item.upc_name);
        }
        if (typeof(prd_item.upc_name) == 'function') {
          prd_item.upc_name(label);
        }
      });
    }

//--------------------------------------------------------------------------------------------------------------------------
    , updateStores: function (data) {
        //console.log('method -- updateStores');
        callback = this;
        if( data.RESULTS != null && data.RESULTS.SUCCESS_CODE == 0) {
          storeList = $('#pl_store_list');
          storeList.empty();
          map = this.map;
          labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
          labelIdx = 0;

          if(data.RESULTS.STORES.STORE.length == 0) {
            var radius = parseInt(data.RESULTS.QUERY.SEARCHRADIUS);
            var radiusMeters = radius *1609.34;

            var radiusCircle = new google.maps.Circle({
              strokeColor: '#FF0000',
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: '#FF0000',
              fillOpacity: 0.35,
              map: map,
              center: map.getCenter(),
              radius: radiusMeters
            });

            this.map.fitBounds(radiusCircle.getBounds());

            storeList.html('No stores found selling within '+ radius.toString() +' miles radius<br/>');
            radius += 30;
            for(idx=radius; idx<=100; idx += 30) {
            //if ( radius <= 100 ) {
              $('<a>',{}).appendTo(storeList).html('Expand search radius to ' + idx.toString() + ' miles?').click(function(){
                radiusCircle.setMap(null);
                callback.radius = idx;
                callback.findLocations();
              });
              $('<br>').appendTo(storeList);
            //}
            }

          } else {

          $.each(data.RESULTS.STORES.STORE, function(idx,item) {
            store = $('<div>',{ 'id': 'pl_sid_' + item.STORE_ID,'class':'pl_store_block'}).appendTo(storeList);
            store.css('overflow','none');
            //REMOVE store.css({'border':'1px solid', 'border-color':'red','width':'100%','padding':'5px','vertical-align':'top', 'height':'100px','overflow':'none'});

            $('<p>',{'class':'pl_store_name'}).html(item.NAME).appendTo(store); //REMOVE .css({'float':'left','font-weight':'bold'});
            $('<p>',{'class':'pl_milage'}).html(Math.round(item.DISTANCE *100)/100 +' Miles').appendTo(store); //REMOVE .css('float','right');
            $('<p>',{'class':'pl_address'}).html(item.ADDRESS +'<br/>'+ item.CITY +', '+ item.STATE +' '+ item.ZIP +'<br/>'+ item.PHONE).appendTo(store); //REMOVE.css({'clear':'both','font-size':'small'});
            $('<p>',{'class':'pl_store_menu'}).html('<span class="glyphicon glyphicon-menu-right btn-lg" aria-hidden="true"></span>').appendTo(store); //REMOVE.css({'float':'right','margin-right':'5px','margin-top':'-70px'});

            point  = new google.maps.LatLng(item.LATITUDE, item.LONGITUDE);
            marker = new google.maps.Marker({ position: point, map: map, label: labels[labelIdx++ % labels.length]});
            item.point = point;
            store.click(function(){
              callback.storeDetails(item);
            });
            store.on('mouseover',function() {
              $('#pl_sid_' + item.STORE_ID).addClass('pl_store_hover');
              callback.map.panTo(item.point);
            });
            store.on('mouseout',function() {
              $('#pl_sid_' + item.STORE_ID).removeClass('pl_store_hover');
            });
            google.maps.event.addListener(marker, 'click', function() {
              callback.storeDetails(item);
            });
            google.maps.event.addListener(marker, 'mouseover', function() {
              $('#pl_sid_' + item.STORE_ID).addClass('pl_store_hover');
              storeList.animate({scrollTop: storeList.scrollTop() +  $('#pl_sid_'+item.STORE_ID).position().top },'500');
            });
            google.maps.event.addListener(marker, 'mouseout', function() {
              $('#pl_sid_' + item.STORE_ID).removeClass('pl_store_hover');
           });

          });
        }


        }
                  //Online Retailers
          if ( (data.RESULTS.STORES.COUNT < 4) && (this.params.hasOwnProperty('online_venders')) ) {

            $.each(this.params.online_venders, function(idx,item) {
              var link_div = $('<div>',{'class':'pl_store_block'}).appendTo(storeList);
              $('<p>',{'class':'pl_order_online'}).html("Order Online").appendTo(link_div).css('z-index','3');
              var link_anchor = $('<a>',{'href':item.url, 'target':'_blank'}).appendTo(link_div).css('z-index','1');;
              $('<img>',{'src':item.image, 'class':item.class}).appendTo(link_anchor).css('z-index','1');;
              link_anchor.click(function(){
                var url = callback.baseURL;
                url += 'OnlineStore?sid=' + callback.session_id;
                url += '&client_id=' + callback.client_id;
                url += '&brand_id=' + callback.brand_id;
                url += '&url=' + encodeURI(item.url);
                
                $.getJSON(url);
              });
            });
          }
    }
//--------------------------------------------------------------------------------------------------------------------------
    , renderStoreDetails: function (div) {
        //console.log('method -- renderStoreDetails');
        modal = $('<div>',{'id':'pl_store_detail','class':'modal fade'}).appendTo(div);
        dialog = $('<div>',{'class':'modal-dialog'}).appendTo(modal);
        content = $('<div>',{'class':'modal-content'}).appendTo(dialog);
        header = $('<div>',{'class':'modal-header'}).appendTo(content);
        $('<button>',{'type':'button','class':'close','data-dismiss':'modal','aria-hidden':'true'}).html('&times;').appendTo(header);
        $('<div>',{'id':'pl_store_logo'}).appendTo(header).css({'margin':'auto','width':'100px'});
        $('<h4>',{'id':'pl_store_title','class':'modal-title'}).appendTo(header).css({'margin-top':'-100px'});
        body = $('<div>',{'id':'pl_store_body','class':'modal-body'}).appendTo(content);
        footer = $('<div>',{'id':'pl_store_footer','class':'modal-footer'}).appendTo(content);
    }
//--------------------------------------------------------------------------------------------------------------------------
    , storeDetails: function (item) {
        //console.log('method -- storeDetails');
        logo = $('#pl_store_logo');
        logo.empty();
        title = $('#pl_store_title');
        title.empty();
        body = $('#pl_store_body');
        body.empty();
        footer = $('#pl_store_footer');
        footer.empty();
        $('<p>',{'class':'pl_store_name'}).html(item.NAME).appendTo(title); //REMOVE .css({'font-weight':'bold'})
        $('<p>',{'class':'pl_address'}).html(item.ADDRESS +'<br/>'+ item.CITY +', '+ item.STATE +' '+ item.ZIP +'<br/>'+ item.PHONE).appendTo(title); //REMOVE .css({'font-size':'small'});
        //var imgurl = '//advantage.iriworldwide.com/cicdimage/i?base=' + encodeURI(item.NAME) + '&s=s';
        //$('<img>',{'src':imgurl}).appendTo(logo).fadeIn();
        this.fetchStoreProducts(item);
        $('#pl_store_detail').modal('show');
    }
//--------------------------------------------------------------------------------------------------------------------------
    , fetchStoreProducts: function (item) {
        //console.log('method -- loadingImage');
        callback = this;
        var prod_list = this.prod_list;
        body = $('#pl_store_body');
        footer = $('#pl_store_footer');
        var url = this.baseURL + 'stores?client_id=' + this.client_id +'&brand_id=' + this.brand_id + '&store_id=' + item.STORE_ID +'&output=json';
        url += '&sid=' + this.session_id;
        url += this.group_list;
        this.loadingImage(body);
        $.getJSON( url, function(data){
          body.empty();

          if( data.products != null) {
            var button_label = "See Products Available";
            if(prod_list.length > 0) {

            button_label = "See Other Products Available";

            $('<p>').html('Products Available at this Location:').appendTo(body).css({'font-weight':'bold'});

            $.each(prod_list, function(idx,item) {
              found_item = $('<div>').appendTo(body);
              var group = $.unique($.map(callback.product_info.products.product, function(element){ if (element.group_id == item ) return element.group_id; }));
              var sold = $.grep(data.products.product, function (element) {
                return (element.upc_code == item || ($.inArray(item,group) != -1) );
              });
              if( sold.length > 0 ) {
                $('<span>',{'class':'glyphicon glyphicon glyphicon-ok-circle pl_product_available', 'info':'Available at this location'}).appendTo(found_item);
              } else {
                $('<span>',{'class':'glyphicon glyphicon glyphicon-remove-circle pl_product_not_available', 'info':'Not available at this location'}).appendTo(found_item);
              }
              found_item.append(callback.upc_info[item]);
            });
            $('<br>').appendTo(body);

            }

            var other = $('<div>',{'class':'container'}).appendTo(footer);
            $('<button>',{'type':'button','class':'btn btn-default collapsed','data-toggle':'collapse','data-target':'#otherproducts'}).html(button_label).appendTo(other);
            var otherProducts = $('<div>',{'id':'otherproducts','class':'collapse'}).appendTo(other);

            $.each(data.products.product, function(idx,item) {
              callback.renderProductGroup(otherProducts, item);
            });
          }
        });
    }
//--------------------------------------------------------------------------------------------------------------------------
    , renderProductGroup: function (div, upc) {
        var info = $.grep(this.product_info.products.product, function (element) {
          return (element.upc_code == upc.upc_code);
        });

        $.each(info, function(idx,item) {
          var group = $('#otherGroup'+ item.group_id);
          if (group.length == 0) {
            $('<p>',{'class':'pl_other_product_heading'}).html(item.group_name).appendTo(div);
            group = $('<ul>',{'id':'otherGroup' + item.group_id, 'class':'pl_other_product_list'}).appendTo(div);
          }
          $('<li>',{'class':'pl_other_product'}).appendTo(group).html(item.upc_name);
        });
    }
//--------------------------------------------------------------------------------------------------------------------------
    , loadingImage: function (div) {
        //console.log('method -- loadingImage');
        div.empty();
        var loader = $('<div>',{'id':'pl_load_container'}).appendTo(div); //REMOVE .css({'text-align':'center'});
        $('<label>',{'id':'pl_load_indicator','for':'load'}).html('Searching').appendTo(loader); //REMOVE .css({'margin':'auto'});
        $('<img>',{'id':'pl_load_image','src': './images/siglogo-light.gif', 'class':'img-responsive'}).appendTo(loader); //REMOVE .css({'margin':'auto'});
    }
//--------------------------------------------------------------------------------------------------------------------------
    , createMap: function() {
      //console.log('method -- createMap');
        this.centerPnt = new google.maps.LatLng(this.latitude, this.longitude);
        var mapCanvas = $('#pl_map_content')[0];
        var mapOptions = { center: this.centerPnt, zoom: 12, mapTypeId: google.maps.MapTypeId.ROADMAP };
        var map = new google.maps.Map(mapCanvas, mapOptions);
        this.map = map;
        var marker = new google.maps.Marker({ position: this.centerPnt, map: this.map, title: 'center'});                 
    }
//--------------------------------------------------------------------------------------------------------------------------
    , criticalError: function ( msg ) {
      this.bscontainer.empty();
      this.bscontainer.html('Error -- ' + msg);
      throw 'Error -- ' + msg;
    }

};

})();