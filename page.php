<?php get_header(); ?>

  <div class="default">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div class="default__hero hero">
      
      <div class="default__hero-inner">
        
        <h1 style="color:<?php the_field('hero_line_1_color'); ?>;"><?php the_field('hero_line_1'); ?><?php if(get_field('hero_line_2')) { echo ' <span style="color: '. get_field('hero_line_2_color') .'">'; the_field('hero_line_2'); echo '</span>'; } ?>
        
      </div>

    </div>

    <div class="default__body">
      
      <div class="default__body-inner">
        <?php the_content(); ?>
      </div>

      <div style="clear: both;"></div>

      <a class="down">Scroll</a>

    </div>
    
    <div id="lower">
    <?php if(get_field('display_featured_products') != 'no') { ?>
    <div class="products__carousel default__featured">

      <div class="products__carousel-inner">
      
          <?php 

          $featured_products = get_field('featured_products');

          if( $featured_products ): foreach( $featured_products as $post): setup_postdata($post);
            get_template_part('parts/loop','product');
          endforeach; wp_reset_postdata(); endif; ?>

      </div>

    </div>
    <?php } ?>
    
    <?php if(get_field('display_callout') != 'no') { ?>
    <div class="products__block">

      <div class="products__block-inner">

        <div class="products__block-padding">
        
          <?php $block_image = get_field('callout_image'); ?>
          <div data-mh="vcblock" class="products__block-image">
              <img src="<?php echo $block_image['sizes']['content-wide']; ?>" alt="<?php echo $block_image['alt']; ?>" />
          </div>

          <div data-mh="vcblock" class="products__block-text">

            <div class="products__block-cell">
                  
              <h4><?php the_field('callout_subheader'); ?></h4>

              <h3><?php the_field('callout_header'); ?></h3>
                    
              <?php $link_type = get_field('callout_button_link_type');
              if ( $link_type == 'pdf') { ?>
              <a class="button" href="<?php the_field('callout_button_pdf'); ?>" download>
              <?php } else { ?>
              <a class="button" href="<?php the_field('callout_button_link'); ?>" <?php if($link_type == 'external-link') { echo 'target="_blank" rel="noopener"'; } ?>>
                    <?php } ?>
                    <?php the_field('callout_button_text'); ?></a>

            </div>

          </div>

        </div>

      </div>
            
    </div>
    <?php } ?>
    
    <?php if(get_field('show_carousel') != 'no') { ?>
    <div class="default__carousel">

      <h3><?php the_field('products_carousel_headline'); ?></h3>
        
      <?php get_template_part('parts/product-carousel'); ?>
        
      <a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>products/">Explore All Products</a>

    </div>
    <?php } ?>

    </div>
    
    <?php endwhile; endif; ?>

  </div>

<?php get_footer(); ?>