<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div class="contact">

    <div class="contact__hero hero">

    </div>

    <div class="contact__body">
      
      <div class="contact__text">
        
        <h1><?php the_title(); ?></h1>

        <?php the_content(); ?>

      </div>

      <div class="contact__form">
        
        <?php if(shortcode_exists('gravityform')) {
          echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true" tabindex="10"]');
        } ?>

      </div>

    </div>

  </div>
  
  <?php endwhile; endif; ?>

<?php get_footer(); ?>