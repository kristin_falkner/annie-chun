<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div class="homepage">

    <div class="homepage__slider">
      
      <?php $i = 1; if( get_field('slides') ): while( has_sub_field('slides') ): 
      $slide_image = get_sub_field('slide_image'); ?>
        
      <div class="homepage__slide">

        <div class="homepage__slide-image homepage-slide-image-<?php echo $i; ?>"></div>
        
        <div class="homepage__slide-inner">

          <div class="homepage__slide-text <?php if(get_sub_field('slide_product_image')) { echo 'homepage__slide-text-product'; } ?>">
              
            <h2 style="color: <?php the_sub_field('slide_headline_line_1_color'); ?>"><?php the_sub_field('slide_headline_line_1'); ?> <span class="<?php the_sub_field('slide_headline_dot_border'); ?>"  style="color:<?php the_sub_field('slide_headline_line_2_color'); ?>"><?php the_sub_field('slide_headline_line_2'); ?></span></h2>

            <p style="color: <?php the_sub_field('slide_text_color'); ?>">
              <?php the_sub_field('slide_text'); ?>
              <a class="go-btn" href="<?php the_sub_field('slide_link'); ?>">GO</a>
            </p>

          </div>

        </div>

        <?php if(get_sub_field('slide_product_image')) {
        $slide_product = get_sub_field('slide_product_image');
        $slide_width = $slide_product['width'] / 2; ?>
        <div class="homepage__slide-product">
          <div class="homepage__slide-product-inner">
            <a href="<?php the_sub_field('slide_link'); ?>"><img width="<?php echo $slide_width; ?>" src="<?php echo $slide_product['url']; ?>" alt="<?php echo $slide_product['alt']; ?>" /></a>
          </div>
        </div>
        <?php } ?>

      </div>
       
      <?php $i++; endwhile; endif; ?>

    </div>

    <div class="homepage__products">

      <div class="homepage__products-inner">
      
        <div class="homepage__products-intro">
          <?php the_content(); ?>
        </div>

        <?php get_template_part('parts/product-carousel'); ?>

        <a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>products/">Explore All Products</a>

      </div>

    </div>
    
    <div class="homepage__blocks">
      
      <?php if( get_field('variable_content_blocks') ): while( has_sub_field('variable_content_blocks') ): 
      $block_image = get_sub_field('block_image'); ?>
        
        <div class="homepage__block">

          <div class="homepage__block-inner">

            <div class="homepage__block-padding">

              <div data-mh="vcblock" class="homepage__block-image">
                <img src="<?php echo $block_image['sizes']['content-wide']; ?>" alt="<?php echo $block_image['alt']; ?>" />
              </div>

              <div data-mh="vcblock" class="homepage__block-text">

                <div class="homepage__block-cell">
                
                  <h4><?php the_sub_field('block_subheader'); ?></h4>

                  <h3><?php the_sub_field('block_header'); ?></h3>
                  
                  <?php $link_type = get_sub_field('block_button_link_type');
                  if ( $link_type == 'pdf') { ?>
                  <a class="button" href="<?php the_sub_field('block_button_pdf'); ?>" download>
                  <?php } else { ?>
                  <a class="button" href="<?php the_sub_field('block_button_link'); ?>" <?php if($link_type == 'external-link') { echo 'target="_blank" rel="noopener"'; } ?>>
                  <?php } ?>
                  <?php the_sub_field('block_button_text'); ?></a>

                </div>

              </div>

            </div>
          
          </div>

        </div>
       
      <?php endwhile; endif; ?> 

    </div>
   
  </div>
  
  <?php endwhile; endif; ?>

<?php get_footer(); ?>