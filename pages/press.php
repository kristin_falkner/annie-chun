<?php
/*
Template Name: Press
*/
?>

<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div class="press">

    <div class="press__hero hero">
      
      <div class="press__hero-inner">
        
        <h1><?php the_field('hero_line_1'); ?><?php if(get_field('hero_line_2')) { echo ' <span>'; the_field('hero_line_2'); echo '</span>'; } ?>
        
      </div>

    </div>

    <div class="press__intro">
      <?php the_content(); ?>
    </div>
    
    <div class="press__grid">
      
      <div class="press__grid-inner">
        
        <?php $press_query = "post_type=press&orderby=menu_order&order=ASC&posts_per_page=-1"; $press_query = new WP_Query($press_query); ?>
        <?php if ($press_query->have_posts()) : while ($press_query->have_posts()) : $press_query->the_post();
          get_template_part('parts/loop','press');
        endwhile; endif; wp_reset_query(); ?>

      </div>

    </div>

    <div class="press__cta">
      
      <div class="press__cta-inner">

        <h2><?php the_field('lower_cta_headline'); ?></h2>

        <?php the_field('lower_cta_text'); ?>

      </div>

    </div>

  </div>
  
  <?php endwhile; endif; ?>

<?php get_footer(); ?>