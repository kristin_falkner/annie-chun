<?php
/*
Template Name: Products
*/
?>

<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div class="products-landing">

    <div class="products__hero hero">
      
      <div class="products__hero-inner products__hero-landing">
        
        <h1><?php the_field('hero_line_1'); ?><?php if(get_field('hero_line_2')) { echo ' <span>'; the_field('hero_line_2'); echo '</span>'; } ?>
        
      </div>

    </div>

    <div class="products__landing-body">

      <div class="products__mobile-trigger">Product Categories <span>+</span></div>

      <div class="products__mobile">

        <ul>
        <?php
         
        // Gets every "category" (term) in this taxonomy to get the respective posts
        $terms = get_terms( array(
          'taxonomy' => 'products',
          'orderby' => 'term_id',
          'order' => ASC
        ) );
         
        foreach( $terms as $term ) : ?>
        <li><a href="#<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li>
        <?php endforeach; ?>
        <li class="mobile-close">Close</li>
        </ul>

      </div>

      <div class="products__landing-nav">
        
        <ul>
        <?php
         
        // Gets every "category" (term) in this taxonomy to get the respective posts
        $terms = get_terms( array(
          'taxonomy' => 'products',
          'hide_empty' => false,
          'orderby' => 'term_id',
          'order' => ASC
        ) );
         
        foreach( $terms as $term ) : ?>
        <li><a href="#<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li>
        <li class="bullet">&bull;</li>
        <?php endforeach; ?>
        </ul>

      </div>

      <div class="products__landing">

        <?php
         
        // Gets every "category" (term) in this taxonomy to get the respective posts
        $terms = get_terms( array(
          'taxonomy' => 'products',
          'orderby' => 'term_id',
          'order' => ASC
        ) );
         
        foreach( $terms as $term ) : ?>
         
        <div id="<?php echo $term->slug; ?>" class="offset"></div>
        <div class="products__group">

          <div class="products__group-top">

            <div class="products__group-description">
              <h2><?php echo $term->name; ?></h2>

              <p><?php echo $term->description; ?></p>

            </div>
            
            <?php
            // vars
            $product_icons = get_field('product_category_image_attributes','products_'. $term->term_id);

            // check
            if( $product_icons ): ?>
            <div class="products__group-icons">
              <ul>
                <?php foreach( $product_icons as $product_icon ): ?>
                  <li class="<?php echo $product_icon['value']; ?>"><span class="<?php echo $product_icon['value']; ?>"><?php echo $product_icon['label'] ?></span></li>
                <?php endforeach; ?>
                <?php if(get_field('ready_in_minutes')) { ?>
                  <li class="minutes">Ready in <br/><?php the_field('ready_in_minutes'); ?> minutes</li>
                <?php } ?>
              </ul>
            </div>
            <?php endif; ?>

          </div>

          <div class="products__carousel">

            <div class="products__carousel-inner">
         
                <?php
                $args = array(
                        'post_type' => 'product',
                        'orderby' => 'menu_order',
                        'order' => 'ASC',
                        'posts_per_page' => -1,  //show all posts
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'products',
                                'field' => 'slug',
                                'terms' => $term->slug,
                            )
                        )
         
                    );
                $product_query = new WP_Query($args);
         
                if( $product_query->have_posts() ): while( $product_query->have_posts() ) : $product_query->the_post();
                  get_template_part('parts/loop','product');
                endwhile; endif; wp_reset_query(); ?>

              </div>

            </div>

          </div>
         
          <?php endforeach; ?>
        

      </div>

    </div>

    <div class="products__block">

      <div class="products__block-inner">

        <div class="products__block-padding">
        
          <?php $block_image = get_field('callout_image'); ?>
          <div data-mh="vcblock" class="products__block-image">
              <img src="<?php echo $block_image['sizes']['content-wide']; ?>" alt="<?php echo $block_image['alt']; ?>" />
          </div>

          <div data-mh="vcblock" class="products__block-text">

            <div class="products__block-cell">
                  
              <h4><?php the_field('callout_subheader'); ?></h4>

              <h3><?php the_field('callout_header'); ?></h3>
                    
              <?php $link_type = get_field('callout_button_link_type');
              if ( $link_type == 'pdf') { ?>
              <a class="button" href="<?php the_field('callout_button_pdf'); ?>" download>
              <?php } else { ?>
              <a class="button" href="<?php the_field('callout_button_link'); ?>" <?php if($link_type == 'external-link') { echo 'target="_blank" rel="noopener"'; } ?>>
                    <?php } ?>
                    <?php the_field('callout_button_text'); ?></a>

            </div>

          </div>

        </div>

      </div>

    </div>
          
  </div>
  
  <?php endwhile; endif; ?>

<?php get_footer(); ?>