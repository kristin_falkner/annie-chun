<?php
/*
Template Name: Recipes
*/
?>

<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div class="recipes">

    <div class="recipes__hero hero">
      
      <div class="recipes__hero-inner">
        
        <h1><?php the_field('hero_line_1'); ?><?php if(get_field('hero_line_2')) { echo ' <span>'; the_field('hero_line_2'); echo '</span>'; } ?>
        
      </div>

    </div>

    <div class="recipes__body">

      <div class="recipes__filter">
        <?php $terms = get_terms( 'recipe-type' );
        if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
          echo '<ul>';
          echo '<li class="label">Filter:</li>';
          echo '<li class="active" data-filter="all">All</li>';
          foreach ( $terms as $term ) {
            echo '<li data-filter="'. $term->term_id .'">' . $term->name . '</li>';
          }
          echo '</ul>';
        } ?>
      </div>

      <div class="recipes__filter-mobile-trigger">Filter Recipes <span>+</span></div>
      
      <div class="recipes__filter-mobile">

        <?php $terms = get_terms( 'recipe-type' );
        if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
          echo '<ul>';
          echo '<li class="active" data-filter="all">All</li>';
          foreach ( $terms as $term ) {
            echo '<li data-filter="'. $term->term_id .'">' . $term->name . '</li>';
          }
          echo '<li class="mobile-close">Close</li>';
          echo '</ul>';
        } ?>

      </div>

      <div class="recipes__grid">
        
        <div class="recipes__grid-inner">

          <div class="filtr-container">
          
          <?php $recipes_query = "post_type=recipes&orderby=menu_order&order=ASC&posts_per_page=-1"; $recipes_query = new WP_Query($recipes_query); ?>
          <?php if ($recipes_query->have_posts()) : while ($recipes_query->have_posts()) : $recipes_query->the_post();
            get_template_part('parts/loop','recipe');
          endwhile; endif; wp_reset_query(); ?>

          </div>

        </div>

      </div>

      <div class="recipes__products">
        
        <div class="recipes__products-inner">

          <h3><?php the_field('product_carousel_headline'); ?></h3>

          <?php get_template_part('parts/product-carousel'); ?>

          <a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>products/">Explore All Products</a>

        </div>

      </div>

    </div>

  </div>
  
  <?php endwhile; endif; ?>

<?php get_footer(); ?>