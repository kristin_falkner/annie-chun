<?php
/*
Template Name: Our Story
*/
?>

<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div class="story">

    <div class="story__hero hero">

      <div class="story__hero-inner">
        
        <div class="story__hero-text">
          <?php the_content(); ?>
        </div>

      </div>

    </div>

    <div class="story__separator"></div>

    <div id="lower" class="story__body">

      <?php $i = 1; if( get_field('story_sections') ): while( has_sub_field('story_sections') ): ?>
        
      <div class="story__section">

        <div class="story__section-inner story__section-inner-<?php echo $i; ?>">
          
          <div class="story__section-text">
              
            <h2><?php the_sub_field('headline_color_one'); ?> <span><?php the_sub_field('headline_color_two'); ?></span></h2>

            <?php the_sub_field('section_text'); ?>

          </div>

        </div>

        <div class="story__section-image story__section-image-<?php echo $i; ?>"></div>

      </div>
       
      <?php $i++; endwhile; endif; ?> 

    </div>

    <div class="story__endcap">
      
      <div class="story__endcap-inner">
        
        <h2><?php the_field('lower_text'); ?></h2>

      </div>

    </div>

    <div class="story__endcap--mobile">
      <?php the_field('lower_text'); ?>
    </div>

    <div class="story__products">
      
      <div class="story__products-inner">
        
        <h3><?php the_field('products_text'); ?></h3>

        <?php get_template_part('parts/product-carousel'); ?>

        <a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>products/">Explore All Products</a>
      </div>

    </div>

  </div>
  
  <?php endwhile; endif; ?>

<?php get_footer(); ?>