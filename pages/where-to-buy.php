<?php
/*
Template Name: Where to Buy
*/
?>

<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div class="wtb">

    <div class="wtb__hero hero">

    </div>

    <div class="wtb__body content-wrapper">

      <div class="wtb__body-inner">
      
        <div class="gdlr-content">
          
          <div id="TOOL"></div>               
      
        </div>

      </div>

    </div>

    <div class="wtb__callout">
      
      <div class="wtb__callout-inner">
        
        <div class="wtb__callout-text">
          <?php the_field('callout_text'); ?>
        </div>

        <div class="wtb__callout-btn">
          <a class="button" href="<?php the_field('callout_button_link'); ?>" target="_blank" rel="noopener"><?php the_field('callout_button_text'); ?> <i class="fa fa-caret-right"></i></a>
        </div>

      </div>

    </div>

  </div>
  
  <?php endwhile; endif; ?>

<?php get_footer(); ?>