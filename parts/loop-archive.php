<div id="post-<?php the_ID(); ?>" <?php post_class('archive-page__preview'); ?>>
            
  <div class="archive-page__preview-thumb">
              
    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('news-thumbnail'); ?></a>

  </div>

  <div class="archive-page__preview-info">

    <p class="post-detail__meta"><?php the_time('F d, Y'); ?></p>

    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

  </div>

</div>