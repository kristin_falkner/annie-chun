<div class="press__item">
  
  <?php $press_type = get_field('press_link_type');
  if ( $press_type == 'pdf' ) { ?>
  <a href="<?php the_field('press_pdf'); ?>" target="_blank">
  <?php } else { ?>
  <a href="<?php the_field('press_link'); ?>" target="_blank" rel="noopener">
  <?php } ?>
  <div class="press__item-inner">
    
    <?php $press_logo = get_field('press_logo'); ?>
    <img src="<?php echo $press_logo['sizes']['press-thumb']; ?>" alt="<?php the_title(); ?>" />
    
  </div>
  </a>

</div>