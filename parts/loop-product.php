<div data-mh="product-group" class="products__carousel-item">
  <?php $flavor_name = get_field('flavor_name');
      $flavor_name = sanitize_title($flavor_name);
      $terms = get_the_terms($post->ID, 'products');
      if (! empty($terms)) {
        foreach ($terms as $term) {
          $url = get_term_link($term->slug, 'products');
        }
      } ?>
  <a href="<?php echo $url; ?>#<?php echo $flavor_name; ?>">
  <div class="products__carousel-item-inner">
    <?php the_post_thumbnail('product-thumb'); ?>
    <h3><?php the_field('flavor_name'); ?></h3>
  </div>
  </a>

</div>