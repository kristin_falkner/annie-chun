<div class="recipes__item filtr-item" data-category="<?php   // Get terms for post
  $terms = get_the_terms( $post->ID , 'recipe-type' );
  // Loop over each item since it's an array
  if ( $terms != null ){
    $i = 1;
    foreach( $terms as $term ) {
    // Print the name method from $term which is an OBJECT
    print $term->term_id;
    // Comma separator except last item
    echo ($i < count($terms))? ", " : "";
    // Get rid of the other data stored in the object, since it's not needed
    unset($term);
    $i++;
    }
  } ?>">
  
  <a href="<?php the_permalink(); ?>">
  <div class="recipes__item-inner">
    
    <?php the_post_thumbnail('thumbnail'); ?>

    <h3><?php the_title(); ?></h3>

  </div>
  </a>

</div>