<div class="page__navigation">
	<div class="page__navigation-previous"><?php next_posts_link('&laquo; Older Entries') ?></div>
	<div class="page__navigation-next"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
</div>