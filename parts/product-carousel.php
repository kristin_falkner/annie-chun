<div class="product-carousel">

  <div class="product-carousel__inner">
  
  <?php
       
  // Gets every "category" (term) in this taxonomy
  $terms = get_terms( array(
    'taxonomy' => 'products',
    'orderby' => 'term_id',
    'order' => ASC
  ));
       
  foreach( $terms as $term ) :
  $featured = get_field('featured_product_image','products_'. $term->term_id); ?>
  <div class="product-carousel__item">
    
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>products/#<?php echo $term->slug; ?>">
    <div class="product-carousel__item-inner">

      <img src="<?php echo $featured['sizes']['product-thumb']; ?>" alt="<?php echo $term->name; ?>" />
      
      <h4><?php echo $term->name; ?></h4>

    </div>
    </a>

  </div>
  <?php endforeach; ?>

  </div>

</div>