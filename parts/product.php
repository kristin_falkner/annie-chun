<?php $flavor_name = get_field('flavor_name');
      $flavor_name = sanitize_title($flavor_name); ?>
<div class="product__wrapper <?php echo $flavor_name; ?>">

  <?php $queried_object = get_queried_object(); 
        $taxonomy = $queried_object->taxonomy;
        $term_id = $queried_object->term_id; ?>
  <div class="product">

    <div data-mh="product-half" class="product__image">

      <div class="product__image-wrapper">
        <?php the_post_thumbnail('large'); ?>
      </div>

    </div>

    <div data-mh="product-half" class="product__info">
      <?php $thermometer = get_field('thermometer_level'); ?>
      <h2 <?php if ( $thermometer != 'none' ) { echo 'class="thermometer-exists" '; } ?>style="color: <?php the_field('product_color'); ?>;"><?php if(get_field('product_top_line')) { echo '<span class="product__subheader">'; the_field('product_top_line'); echo '</span> '; } ?><?php the_field('flavor_name'); ?><?php if(get_field('product_bottom_line')) { echo ' <span class="product__headertype">'; the_field('product_bottom_line'); echo '</span>'; } ?></h2>

      <div class="product__description">
        <?php the_content(); ?>

        <?php if ( $thermometer != 'none' ) { ?>
        <div class="product__thermometer <?php the_field('thermometer_level'); ?>"></div>
        <?php } ?>
      </div>

      <ul class="product__purchase">
        <?php if(get_field('hide_in_store_link') != 'yes') { ?>
        <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>findus/">Buy in Store <i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
        <?php } ?>
        <?php if(get_field('amazon_purchase_link')) { ?>
        <li><a href="<?php the_field('amazon_purchase_link'); ?>" target="_blank" rel="noopener">Buy on Amazon <i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
        <?php } ?>
      </ul>

    </div>

  </div>

  <div class="product__bar" style="background-color: <?php the_field('product_color'); ?>;">

    <div class="product__bar-inner">

      <div class="product__nutrition">
        
        <?php if(get_field('nutrition_facts')) {
          $facts_image = get_field('nutrition_facts'); ?>
        <a class="button button--dotted nut-info" href="#" data-caption="<?php the_field('nutritional_information_disclaimer','option'); ?>" data-featherlight="<?php echo $facts_image['url']; ?>">Nutritional Information</a>
        <?php } ?>

      </div>
      
      <div class="product__details">

        <div class="product__stats">
          <?php
          // vars
          $product_icons = get_field('product_image_attributes');

          // check
          if( $product_icons ) { ?>
          <ul>
            <?php foreach( $product_icons as $product_icon ): ?>
              <li class="<?php echo $product_icon['value']; ?>"><span class="product-icon <?php echo $product_icon['value']; ?>"><?php echo $product_icon['label'] ?></span></li>
            <?php endforeach; ?>
          <?php } else { ?>
          <ul class="minutes-only">
          <?php } ?>
          <?php $minutes = get_field('ready_in_minutes');
          if( $minutes ) { ?>
            <li class="minutes">Ready in <span><?php echo $minutes; ?> minute<?php if( $minutes != '1' ) { echo 's'; } ?></span></li>
          <?php } ?>
          </ul>

        </div>
        
        <div class="product__ingredients">
          
          <?php the_field('ingredients'); ?>

        </div>

        <div class="product__nutrition--mobile">
          <?php if(get_field('nutrition_facts')) {
          $facts_image = get_field('nutrition_facts'); ?>
          <a class="button button--dotted nut-info" href="#" data-caption="<?php the_field('nutritional_information_disclaimer','option'); ?>" data-featherlight="<?php echo $facts_image['url']; ?>">Nutritional Information</a>
          <?php } ?>
        </div>

      </div>

    </div>

  </div>

</div>