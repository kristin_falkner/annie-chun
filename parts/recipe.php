<div class="recipe" itemscope itemtype="http://schema.org/Recipe">

  <div class="recipe__hero-wrapper">

    <div class="recipe__hero hero"></div>
      
    <div class="recipe__hero-inner">
        
      <div class="recipe__hero-box">

        <h1 itemprop="name"><?php the_title(); ?></h1>

        <div itemprop="description">
          <?php the_content(); ?>
        </div>

      </div>

      <div class="recipe__hero-image">
        <img itemprop="image" src="<?php the_post_thumbnail_url('background-mobile'); ?>"
       alt="<?php $alt = get_the_title(); echo esc_attr( $alt ); ?>" />
      </div>

    </div>

  </div>

  <div class="recipe__bar">

    <div class="recipe__bar-inner">

      <div class="recipe__bar-pad">

      <div data-mh="recipe-spec" itemprop="recipeYield" class="recipe__spec recipe__servings">
        Recipe Serves
        <span><?php the_field('servings'); ?></span>
        <div class="recipe__spec-divider"><img src="<?php echo get_template_directory_uri(); ?>/images/dots-brown-vertical.svg" alt="" /></div>
      </div>

      <div data-mh="recipe-spec" class="recipe__spec recipe__prep">
        <meta itemprop="prepTime" content="PT<?php the_field('prep_time'); ?>M">
        Minutes Prep
        <span><?php the_field('prep_time'); ?></span>
        <div class="recipe__spec-divider"><img src="<?php echo get_template_directory_uri(); ?>/images/dots-brown-vertical.svg" alt="" /></div>
      </div>

      <div data-mh="recipe-spec" class="recipe__spec recipe__cook">
        <meta itemprop="cookTime" content="PT<?php the_field('cook_time'); ?>M">
        Minutes Cook
        <span><?php the_field('cook_time'); ?></span>
        <div class="recipe__spec-divider"><img src="<?php echo get_template_directory_uri(); ?>/images/dots-brown-vertical.svg" alt="" /></div>
      </div>

      <div data-mh="recipe-spec" class="recipe__spec recipe__total">
      <meta itemprop="totalTime" content="PT<?php the_field('total_time'); ?>M">
        Total Minutes
        <span><?php the_field('total_time'); ?></span>
        <div class="recipe__spec-divider"><img src="<?php echo get_template_directory_uri(); ?>/images/dots-brown-vertical.svg" alt="" /></div>
      </div>

      <div class="recipe__difficulty">
        Difficulty: <?php the_field('difficulty'); ?>
      </div>

      </div>

    </div>

  </div>

  <div class="recipe__content">

    <div class="recipe__ingredients">
      
      <h2><?php the_field('ingredients_header','option'); ?></h2>

      <?php $ingredients = get_field('ingredients');
      $ingredients_schema = str_replace('<li>','<li itemprop="recipeIngredient">', $ingredients);
      echo $ingredients_schema; ?>

    </div>

    <div class="recipe__method">
      
      <div class="recipe__method-title">

        <h2><?php the_field('method_header','option'); ?></h2>

        <div class="recipe__share recipe__share--desktop">
          <?php get_template_part('parts/social-share'); ?>
        </div>

      </div>

      <?php the_field('method'); ?>

      <?php 

      $posts = get_field('products_used');

      if( $posts ): ?>
          <ul class="recipe__products">
          <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
              <?php setup_postdata($post);
              $flavor_name = get_field('flavor_name');
              $flavor_name = sanitize_title($flavor_name);
              $terms = get_the_terms( $post->ID, 'products' );
              $term = array_pop($terms);
              ?>
              <li>
                  <a href="<?php echo get_term_link($term->slug, 'products'); ?>#<?php echo $flavor_name; ?>"><?php the_post_thumbnail('product-thumb') ?></a>
              </li>
          <?php endforeach; ?>
          </ul>
          <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
      <?php endif; ?>

      <div class="recipe__share recipe__share--mobile">
        <?php get_template_part('parts/social-share'); ?>
      </div>

    </div>

  </div>

  <?php $args = array(
    'post_type' => 'recipes',
    'posts_per_page' => 3,
    'post__not_in' => array( $post->ID ),
    'orderby' => 'rand',
    'order' => 'ASC'
  ); $more_recipes = new WP_Query($args); ?>
  <?php if ($more_recipes->have_posts()) : ?>

  <div class="recipe__related">
            
    <div class="recipe__related-grid">

      <div class="recipe__related-grid-inner">
        
        <?php while ($more_recipes->have_posts()) : $more_recipes->the_post();
          get_template_part('parts/loop','recipe');
        endwhile;  ?>
      
      </div>

    </div>
            
    <a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>recipes/">All Recipes</a>

  </div>

  <?php endif; wp_reset_query(); ?>

</div>