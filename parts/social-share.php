<ul>
  <li class="facebook"><a class="fb-share" href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/Recipe_Icon_Facebook.svg" alt="Share on Facebook" /></a></li>
  <li><a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php the_post_thumbnail_url('large'); ?>&description=<?php the_title(); ?>" data-pin-custom="true"><img src="<?php echo get_template_directory_uri(); ?>/images/Recipe_Icon_Pinterest.svg" alt="Pin on Pinterest" /></a></li>
  <li><a href="mailto:?subject=Check out this recipe&amp;body=<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/Recipe_Icon_Email.svg" alt="Share via Email" /></a></li>
  <li><a href="javascript:window.print()"><img src="<?php echo get_template_directory_uri(); ?>/images/Recipe_Icon_Print.svg" alt="Print Recipe" /></a></li>
</ul>