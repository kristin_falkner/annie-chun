<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); 
  
    if(is_singular('recipes')) {
      get_template_part('parts/recipe');
    } else {
      get_template_part('parts/loop','single');
    }
  
  endwhile; else: ?>

    <p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>

<?php get_footer(); ?>
