(function($) {

  $(document).ready(function(){

    // mobile menu
    $('.header__mobile').on('click',function(){
      $('.mobile-menu').slideToggle();
      $(this).children('.header__icon').toggleClass('open');
    });

    // fixed header
    var header = $('header');
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 100) {
            header.removeClass('full').addClass('shrink');
        } else {
            header.removeClass('shrink').addClass('full');
        }
    });

    // homepage slider
    $('.homepage__slider').slick({
      arrows: false,
      dots: true,
      autoplay: true,
      autoplaySpeed: 6000,
      adaptiveHeight: true
    });

    // story scroll
    $('.down').click(function(){
      $('html, body').animate({
        scrollTop: $('#lower').offset().top
      }, 500);
    });

    // product carousel
    $('.product-carousel__inner').slick({
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 640,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    // recipe filtering
    $('.recipes__filter li').on('click',function(){
      $('.recipes__filter li').removeClass('active');
      $(this).addClass('active');
    });

    $('.recipes__item-inner').matchHeight({
      byRow: false
    });

    if ($('div.filtr-container').length) {

      $('div.filtr-container').imagesLoaded( function() {
        $.fn.matchHeight._update();
        var filtrizr = $('.filtr-container').filterizr('setOptions', {layout: 'sameSize'});

        filtrizr.on('filteringEnd', function() {
          $.fn.matchHeight._update();
        });
      });
    }

    // recipe mobile
    $('.recipes__filter-mobile-trigger').on('click',function(){
      $('.recipes__filter-mobile').slideToggle();
      var text = $(this).children('span').text() == '+' ? '-' : '+';
      $('.recipes__filter-mobile-trigger span').text(text);
    });

    $('.recipes__filter-mobile li a').on('click',function(){
      $(this).parent().removeClass('active');
      $(this).parent().addClass('active');
      $('.recipes__filter-mobile').slideUp();
      $('.recipes__filter-mobile-trigger span').text('+');
    });

    // products mobile
    $('.products__mobile-trigger').on('click',function(){
      $('.products__mobile').slideToggle();
      var text = $(this).children('span').text() == '+' ? '-' : '+';
      $('.products__mobile-trigger span').text(text);
    });

    $('.products__mobile li a').on('click',function(){
      $(this).parent().removeClass('active');
      $(this).parent().addClass('active');
      $('.products__mobile').slideUp();
      $('.products__mobile-trigger span').text('+');
    });

    // product mobile
    $('.product__mobile-trigger').on('click',function(){
      $('.product__mobile').slideToggle();
      var text = $(this).children('span').text() == '+' ? '-' : '+';
      $('.product__mobile-trigger span').text(text);
    });

    $('.product__mobile li').on('click',function(){
      $('.product__mobile li').removeClass('active');
      $(this).addClass('active');
      $('.product__mobile').slideUp();
      $('.product__mobile-trigger span').text('+');
    });

    // products smooth scroll
    $('.products__landing-body a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 500);
          return false;
        }
      }
    });

    // Facebook modal share
    $('.fb-share').click(function(e) {
        e.preventDefault();
        window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });

    // Product flavors
    var hash = window.location.hash.substr(1);
    if ( hash && $('div.product__wrapper').length) {
      $('.'+hash).addClass('active');
      $('.products__nav a').each(function(){
         var myHref = $(this).attr('href');
         var url = '#'+hash;
         if( url == myHref) {
            $(this).addClass('active');
         }
      });
    } else if ($('div.product__wrapper').length) {
      $('.products__nav li:eq(1) a').addClass('active');
      $('.product__wrapper:eq(0)').addClass('active');
    }

    $('.products__nav a, .product__mobile a').on('click',function(){
      var activePanel = $(this).attr('href');
      activePanel = activePanel.replace('#','');
      $('.product__wrapper').removeClass('active');
      $('.products__nav a').removeClass('active');
      $('.'+activePanel).addClass('active');
      $(this).addClass('active');
    });

    // remove bullet
    var lastElement = false;
    $('ul > li').each(function() {
        if (lastElement && lastElement.offset().top != $(this).offset().top) {
            lastElement.addClass('nobullet');
        }
        lastElement = $(this);
    }).last().addClass('nobullet');

    $(window).resize(function(){
      $('li').removeClass('nobullet');
      var lastElement = false;
      $('ul > li').each(function() {
        if (lastElement && lastElement.offset().top != $(this).offset().top) {
            lastElement.addClass('nobullet');
        }
        lastElement = $(this);
      }).last().addClass('nobullet');
    });

    // nutritional info caption
    $('.nut-info').on('click',function(){
      var lightboxCaption = $(this).data('caption');
      $('<div class="caption">'+ lightboxCaption +'</div>').appendTo('.featherlight-content');
    });

    // replace Cabin @
    $('a:contains("@")').html(function(_, html) {
      return html.replace(/(@)/g, '<span class="at-sign">$1</span>');
    });

    if( $('#pl_load_image').length ) {
      // product locator
      $('img#pl_load_image').bind('error', function () {
        $(this).attr('src', 'http://anniechun.com/wp-content/uploads/2016/10/siglogo-light.gif');
      });
    }

    // styling aides
    $('<div style="clear: both;"></div>').insertAfter('.right-half');
    $('<div style="clear: both;"></div>').insertAfter('.right-quarter');
    $('<div style="clear: both;"></div>').insertAfter('.example-link.manufacturing');
    
  });

})(jQuery);
