<?php get_header(); ?>

  <?php $queried_object = get_queried_object(); 
      $taxonomy = $queried_object->taxonomy;
      $term_id = $queried_object->term_id; ?>

  <div class="products__hero hero">

    <div class="products__hero-inner">
        
      <h1 style="color: <?php the_field('hero_line_1_color', $taxonomy . '_' . $term_id); ?>"><?php the_field('hero_line_1', $taxonomy . '_' . $term_id); ?><?php if(get_field('hero_line_2', $taxonomy . '_' . $term_id)) { echo ' <span style="color: '. get_field('hero_line_2_color', $taxonomy . '_' . $term_id) .'">'; the_field('hero_line_2', $taxonomy . '_' . $term_id); echo '</span>'; } ?>
        
    </div>

  </div>

  <div class="products__content">

    <div class="products__nav">
      
      <ul>
        <li class="label">Flavors:</li>
        <?php
        $args = array(
          'post_type' => 'product',
          'posts_per_page' => -1,
          'orderby' => 'menu_order',
          'order' => 'ASC',
          'tax_query' => array(
              array(
              'taxonomy' => 'products',
              'field' => 'id',
              'terms' => $term_id
               )
            )
        );
        $flavor_menu = new WP_Query($args); ?>
        <?php if ($flavor_menu->have_posts()) : while ($flavor_menu->have_posts()) : $flavor_menu->the_post();
        $flavor_name = get_field('flavor_name');
        $flavor_name = sanitize_title($flavor_name); ?>
        <li><a href="#<?php echo $flavor_name; ?>"><?php the_field('flavor_name'); ?></a></li>
        <?php endwhile; endif; wp_reset_query(); ?>
      </ul>

    </div>

    <div class="product__mobile-trigger">
      <?php echo $flavor_menu->post_count; ?> Product Flavors <span>+</span>
    </div>

    <div class="product__mobile">

      <ul>
        <?php
        $args = array(
          'post_type' => 'product',
          'posts_per_page' => -1,
          'orderby' => 'menu_order',
          'order' => 'ASC',
          'tax_query' => array(
              array(
              'taxonomy' => 'products',
              'field' => 'id',
              'terms' => $term_id
               )
            )
        );
        $flavor_menu = new WP_Query($args); ?>
        <?php if ($flavor_menu->have_posts()) : while ($flavor_menu->have_posts()) : $flavor_menu->the_post();
        $flavor_name = get_field('flavor_name');
        $flavor_name = sanitize_title($flavor_name); ?>
        <li data-mh="flavor"><a href="#<?php echo $flavor_name; ?>"><?php the_post_thumbnail('product-thumb'); ?> <?php the_field('flavor_name'); ?></a></li>
        <?php endwhile; endif; wp_reset_query(); ?>
        <li class="mobile-close">Close</li>
      </ul>

    </div>

    <div class="products__body">

      <?php global $query_string;
      query_posts( $query_string . '&orderby=menu_order&order=ASC' );
      if (have_posts()) : while (have_posts()) : the_post(); 
        
        get_template_part('parts/product');  
        
      endwhile; endif; ?>

    </div>

    <div class="product__recipe">

      <div class="product__recipe-inner">

        <div data-mh="recipe-box" class="product__recipe-image">
          
          <?php $recipe_image = get_field('recipe_image', $taxonomy . '_' . $term_id); ?>
          <a href="<?php the_field('recipe_link', $taxonomy . '_' . $term_id); ?>"><img src="<?php echo $recipe_image['sizes']['content-wide']; ?>" alt="<?php echo $recipe_image['alt']; ?>" /></a>

        </div>

        <div data-mh="recipe-box" class="product__recipe-text">

          <div class="product__recipe-cell">
          
          <h4><?php the_field('recipe_subheader', $taxonomy . '_' . $term_id); ?></h4>

          <h3><?php the_field('recipe_header', $taxonomy . '_' . $term_id); ?></h3>

          <a class="button" href="<?php the_field('recipe_link', $taxonomy . '_' . $term_id); ?>">See Recipe</a>

          </div>

        </div>

      </div>

    </div>

    <div class="product__carousel">
      
      <div class="product__carousel-inner">

        <h3><?php the_field('product_carousel_header', $taxonomy . '_' . $term_id); ?></h3>

        <?php get_template_part('parts/product-carousel'); ?>

        <a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>products/">Explore All Products</a>

      </div>

    </div>

  </div>

<?php get_footer(); ?>